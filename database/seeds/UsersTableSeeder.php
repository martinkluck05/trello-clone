<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Martin Gustavo Kluck',
            'email' => 'martinkluck05@gmail.com',
            'password' => bcrypt('mgkluck05'),
        ]);
    }
}
